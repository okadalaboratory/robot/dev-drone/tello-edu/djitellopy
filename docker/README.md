# Dockerを使ってTelloEduを開発する

## インストール
```
$ git pull https://gitlab.com/okadalaboratory/robot/dev-drone/tello-edu/djitellopy.git
$ cd djitellopy
```

### CPU版
```
$ docker compose build tello-djitellpy_cpu
```
```
$ docker compose up tello-djitellpy_cpu -d 
```
```
$ docker compose exec tello-djitellpy_cpu /bin/bash
```

### GPU版
```
$ docker compose build tello-djitellpy_gpu
```
```
$ docker compose up tello-djitellpy_gpu -d 
```
```
$ docker compose exec tello-djitellpy_gpu /bin/bash
```


