#!/usr/bin/env python3
## coding: UTF-8
"""
from https://github.com/damiafuentes/DJITelloPy
"""
from djitellopy import Tello

tello = Tello()

tello.connect()
tello.takeoff()

tello.move_left(100)
tello.rotate_counter_clockwise(90)
tello.move_forward(100)

tello.land()
