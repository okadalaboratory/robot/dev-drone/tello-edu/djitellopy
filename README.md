# DJITelloPyを使ってTelloEDUを飛ばす


## 環境
- Windows10,11
- macOS　Sonoma バージョン14.0
- Ubuntu20.04, 22.04

## インストール
### Python3が動く環境を構築
#### Windows


#### macOS
```
$ sudo apt install python3-pip
$ pip3 install opencv-python opencv-contrib-python numpy
$ pip3 install --upgrade opencv-python
$ pip3 install --upgrade opencv-contrib-python
$ pip3 install --upgrade numpy
```

##### Ubuntu
```
$ sudo apt install python3-pip
$ pip3 install opencv-python opencv-contrib-python numpy
$ pip3 install --upgrade opencv-python
$ pip3 install --upgrade opencv-contrib-python
$ pip3 install --upgrade numpy
```

### DJITelloPyのインストール
```
$ pip3 install djitellopy
```

開発者モードのインストールはこちら
```
git clone https://github.com/damiafuentes/DJITelloPy.git
cd DJITelloPy
pip install -e .
```

##  最初に試すプログラム
広い場所で、周りに人がいないことを確認して実行してください。
```
from djitellopy import Tello

tello = Tello()

tello.connect()
tello.takeoff()

tello.move_left(100)
tello.rotate_counter_clockwise(90)
tello.move_forward(100)

tello.land()
```

## サンプルプログラムの解説
[taking a picture](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/take-picture.py)<br>
垂直に離陸した後、写真を撮影し"picture.png”というファイル名で保存します。

[recording a video](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/record-video.py)<br>
離陸→100cm上昇→半時計周りに一周旋回→着陸のように飛行し、その間のカメラからの動画を"video.avi"というファイル名で保存します。

[flying a swarm (multiple Tellos at once)](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/simple-swarm.py)<br>
３台のドローンを同時に飛行します。

[simple controlling using your keyboard](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/manual-control-opencv.py)<br>
キーボードを使って飛行中のTelloを制御します。

| キー   | 動作      |
| --- | ----------- |
| Esc    | 終了 |
| w    | 前進（３０cm） |
| s    | 後退（３０cm） |
| a    | 左移動（３０cm） |
| d    | 右移動（３０cm） |
| e    | 時計方向に回転（30度） |
| q    | 半時計方向に回転（30度） |
| r    | 上昇（３０cm） |
| f    | 下降（３０cm） |


[mission pad detection](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/mission-pads.py)<br>
ミッションパッドを認識します。

TelloEDUに付属しているミッションパッドは4枚（表裏8枚）で、
- ロケット
- 1から８の星座に見立てた数字
- 惑星の絵

がそれぞれ描かれています。<br>
ロケットの方向が前方のX軸を表しています

プログラムではミッションパッドの情報を画像認識することによって位置を認識させたり、コマンドを実行させたりします。

<img src="fig/tello_7guide.jpg">

[fully featured manual control using pygame](https://github.com/damiafuentes/DJITelloPy/blob/master/examples/manual-control-pygame.py)<br>
Pythonのゲーム開発ライブラリPyGameを使いTelloEDUを制御します。


## OpenCVと連携して画像処理
- ジョイスティックで操縦

- OpenCVを使うプログラムのテンプレート

- ARマーカ認識

- QRコード認識

- 顔検出

- 色検出

- ライントレース

## 複数台のTelloEDUによる編隊飛行
### TelloEDUの設定


## Dockerで試す







## 
[橋口さんの解説](https://qiita.com/hsgucci/items/3327cc29ddf10a321f3c)

- pipでインストールできるpythonライブラリ　[DJITelloPy](https://github.com/damiafuentes/DJITelloPy)　を使う

　名前はTelloPyに似ていますが、バイナリではなくSDKを使うライブラリです。
Python3に対応しており、SDK3.0への対応も行われています。
H.264のデコードはTelloPyと同じPyAVを使っているのでセルフビルドの必要がありません。

また、有志の掲示板に上がっているような隠しコマンドにも対応しているので、下向きカメラへの切り替え機能もあります。
pipでインストールする際には、依存関係にあるOpenCVのインストールも自動で行われます。

以上のことから、
- Python3対応
- SDK3.0対応
- H.264のデコードライブラリで苦労しない
- 環境構築が比較的簡単
の点から、当分の間は　DJITelloPy　が良さそうです。

DJITelloPyのリファレンスマニュアルは　[このページ](https://djitellopy.readthedocs.io/en/latest/)　にあります。

tello.land()
```



